set DXL_BIN=..\..\bin
%DXL_BIN%\deltaxml.exe compare wbw input1.xml input2.xml non-wbw-result.html
%DXL_BIN%\deltaxml.exe compare wbw input1.xml input2.xml wbw-result.html word-by-word=true
%DXL_BIN%\deltaxml.exe compare wbw input1.xml input2.xml orphaned-word-result.html word-by-word=true orphaned-words=true
