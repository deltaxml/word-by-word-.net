set DXL_BIN=..\..\bin
%DXL_BIN%\deltaxml.exe compare dcp-wbw input1.xml input2.xml dcp-non-wbw-result.html
%DXL_BIN%\deltaxml.exe compare dcp-wbw input1.xml input2.xml dcp-wbw-result.html word-by-word=true
%DXL_BIN%\deltaxml.exe compare dcp-wbw input1.xml input2.xml dcp-orphaned-word-result.html word-by-word=true orphaned-words=true
