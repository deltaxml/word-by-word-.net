﻿using DeltaXML.CoreS9Api;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using StaticPDFormatException = com.deltaxml.cores9api.StaticPDFormatException;
using DynamicPDFormatException = com.deltaxml.cores9api.DynamicPDFormatException;
using PDAdvancedConfigException = com.deltaxml.cores9api.PDAdvancedConfigException;
using PDFilterConfigurationException = com.deltaxml.cores9api.PDFilterConfigurationException;
using PipelinedComparatorS9Exception = com.deltaxml.cores9api.PipelinedComparatorS9Exception;



namespace WordByWordUsingDCP
{
    class WordByWordUsingDCP
    {
        public static void Main(string[] args)
        {
            // Find the top level samples directory (whose relative location can vary when run through Visual Studio).
            DirectoryInfo docdir = locateSamplesDirectory("WordByWord");

            // Load the pipeline.dcp file.
            FileInfo dcpFile = new FileInfo(Path.Combine(docdir.FullName, "word-by-word.dcp"));
            DCPConfiguration dcpConfig = null;
            try
            {
                Console.WriteLine("[Step 1 of 3] Initialising DCPConfiguration with the file: " + dcpFile.Name);
                dcpConfig = new DCPConfiguration(dcpFile);
            }
            catch (StaticPDFormatException e)
            {
                handleDcpConfigException(e);
            }           
            catch (FileNotFoundException e)
            {
                handleDcpConfigException(e);
            }

            // Setup the override default value parameter maps for the boolean and string parameters.
            // The DCP file parameters can be overriden by manipulating the parameter map dictionaries.
            // Each of these lines corresponds to one of the DCP file parameters, but with different values.
            IDictionary<string, bool> booleanOverrides = new Dictionary<string, bool>() {
                { "word-by-word", true },
                { "orphaned-words", true },
                { "convert-to-html", false}
            };
            IDictionary<string, string> stringOverrides = new Dictionary<string, string>() {
                { "orphan-length", "2"},
                { "orphan-threshold", "20"}
            };

            // Generating a comparison pipeline
            DocumentComparator dc = null;
            try
            {
                Console.WriteLine("[Step 2 of 3] Generating a DocumentComparator using the DCP configuration");
                dcpConfig.generate(booleanOverrides, stringOverrides);
                dc = dcpConfig.DocumentComparator;
            }
            catch (DynamicPDFormatException e)
            {
                handlePipelineGenerationException(e);
            }
            catch (PDAdvancedConfigException e)
            {
                handlePipelineGenerationException(e);
            }
            catch (PDFilterConfigurationException e)
            {
                handlePipelineGenerationException(e);
            }

            // Provide variables for each of the file-based compare method arguments
            FileInfo inFile1 = new FileInfo(Path.Combine(docdir.FullName, "input1.xml"));
            FileInfo inFile2 = new FileInfo(Path.Combine(docdir.FullName, "input2.xml"));
            FileInfo outFile = new FileInfo(Path.Combine(docdir.FullName, "dcp-result.xml"));

            // Run the compare (with pre/post commentry)
            Console.WriteLine("[Step 3 of 3] Comparing files '" + inFile1 + "' and '" + inFile2 + "'");
            try
            {
                dc.compare(inFile1, inFile2, outFile);
            }
            catch (PipelinedComparatorS9Exception e)
            {
                handleComparisonException(e);
            }
            catch (IOException e)
            {
                handleComparisonException(e);
            }
            Console.WriteLine("Storing results in file '" + outFile + "'");

            // Enable the messages printed to the console/terminal when run from Visual Studio to be read.
            Console.WriteLine();
            Console.WriteLine("Press return to continue");
            Console.ReadLine();
        }

        private static void handleDcpConfigException(Exception e)
        {
            Console.WriteLine("Caught an exception when tryting to load the DCP configuration file.");
            handleException(e);
        }

        private static void handlePipelineGenerationException(Exception e)
        {
            Console.WriteLine("Caught an exception when tryting to generate a comparison pipeline from the DCP configuration.");
            handleException(e);
        }

        private static void handleComparisonException(Exception e)
        {
            Console.WriteLine("Caught an exception when performing the comparison.");
            handleException(e);
        }

        private static void handleException(Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);

            // Enable the error messages printed to the console/terminal when run from Visual Studio to be read.
            Console.WriteLine();
            Console.WriteLine("Press return to continue");
            Console.ReadLine();

            Environment.Exit(1);
        }

        private static DirectoryInfo locateSamplesDirectory(String sampleDir)
        {
            DirectoryInfo docdir = new DirectoryInfo(".");
            while (docdir.Exists && docdir.Name != "samples")
            {
                docdir = docdir.Parent;
            }
            docdir = new DirectoryInfo(Path.Combine(docdir.FullName, sampleDir));
            return docdir;
        }
    }
}
