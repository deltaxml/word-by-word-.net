﻿// Copyright (c) 2013 DeltaXML Ltd.  All rights reserved.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Add System IO for file handling.
using System.IO;

// Add the main DeltaXML .Net API
using DeltaXML.CoreS9Api;

// Add in the exceptions that we explicitly catch within this sample
using DXPConfigurationException = com.deltaxml.core.DXPConfigurationException;
using InvalidPipelineException = com.deltaxml.core.InvalidPipelineException;
using ParserInstantiationException = com.deltaxml.core.ParserInstantiationException;
using PipelinedComparatorException = com.deltaxml.core.PipelinedComparatorException;

using PipelinedComparatorS9Exception = com.deltaxml.cores9api.PipelinedComparatorS9Exception;

using ClassNotFoundException = java.lang.ClassNotFoundException;
using MalformedURLException = java.net.MalformedURLException;

using SaxonApiException = net.sf.saxon.s9api.SaxonApiException;

namespace DotNetSample
{
  class WordByWordUsingDXP
  {
    public static void Main(string[] args)
    {
      // Find the top level samples directory (whose relative location can vary when run through Visual Studio).
      DirectoryInfo docdir= locateSamplesDirectory("WordByWord");
    
      // Load the pipeline.dxp file.
      FileInfo dxpFile= new FileInfo(Path.Combine(docdir.FullName, "word-by-word.dxp"));
      DXPConfigurationS9 dxpConfig= null;
      try {
        dxpConfig= new DXPConfigurationS9(dxpFile);
      } catch (ParserInstantiationException e) {
        handleDxpConfigException(e);
      } catch (InvalidPipelineException e) {
        handleDxpConfigException(e);
      } catch (FileNotFoundException e) {
        handleDxpConfigException(e);
      } catch (SaxonApiException e) {
        handleDxpConfigException(e);
      }

      // Setup the override default value parameter maps for the boolean and string parameters.
      IDictionary<string, bool> booleanOverrides = new Dictionary<string, bool>();
      IDictionary<string, string> stringOverrides = new Dictionary<string, string>();

      // The DXP file parameters can be overriden by manipulating the parameter map dictionaries.
      // Each of these lines corresponds to one of the DXP file parameters, but with different values.
      booleanOverrides["word-by-word"] = true;
      booleanOverrides["orphaned-words"] = true;
      booleanOverrides["convert-to-html"] = false;
      stringOverrides["orphan-length"] = "2";
      stringOverrides["orphan-threshold"] = "20";
    
      // Generating a comparison pipeline
      PipelinedComparatorS9 pc= null;
      try {
        pc= dxpConfig.generate(booleanOverrides, stringOverrides);
      } catch (PipelinedComparatorException e) {
        handlePipelineGenerationException(e);
      } catch (DXPConfigurationException e) {
        handlePipelineGenerationException(e);
      } catch (ClassNotFoundException e) {
        handlePipelineGenerationException(e);
      } catch (MalformedURLException e) {
        handlePipelineGenerationException(e);
      }
    
      // Provide variables for each of the file-based compare method arguments
      FileInfo inFile1= new FileInfo(Path.Combine(docdir.FullName, "input1.xml"));
      FileInfo inFile2= new FileInfo(Path.Combine(docdir.FullName, "input2.xml"));
      FileInfo outFile= new FileInfo(Path.Combine(docdir.FullName, "result.xml"));

      // Run the compare (with pre/post commentry)
      Console.WriteLine("Comparing files '"+inFile1+"' and '"+inFile2+"'");
      try {
        pc.compare(inFile1, inFile2, outFile);
      } catch (PipelinedComparatorS9Exception e) {
        handleComparisonException(e);
      } catch (IOException e) {
        handleComparisonException(e);
      }
      Console.WriteLine("Storing results in file '"+outFile+"'");
  
      // Enable the messages printed to the console/terminal when run from Visual Studio to be read.
      Console.WriteLine();
      Console.WriteLine("Press return to continue");
      Console.ReadLine();
    }

    private static void handleDxpConfigException(Exception e)
    {
      Console.WriteLine("Caught an exception when tryting to load the DXP configuration file.");
      handleException(e);
    }

    private static void handlePipelineGenerationException(Exception e)
    {
      Console.WriteLine("Caught an exception when tryting to generate a comparison pipeline from the DXP configuration.");
      handleException(e);
    }

    private static void handleComparisonException(Exception e)
    {
      Console.WriteLine("Caught an exception when performing the comparison.");
      handleException(e);
    }

    private static void handleException(Exception e)
    {
      Console.WriteLine(e.Message);
      Console.WriteLine(e.StackTrace);

      // Enable the error messages printed to the console/terminal when run from Visual Studio to be read.
      Console.WriteLine();
      Console.WriteLine("Press return to continue");
      Console.ReadLine();

      Environment.Exit(1);
    }

    private static DirectoryInfo locateSamplesDirectory(String sampleDir)
    {
      DirectoryInfo docdir = new DirectoryInfo(".");
      while (docdir.Exists && docdir.Name != "samples")
      {
        docdir = docdir.Parent;
      }
      docdir = new DirectoryInfo(Path.Combine(docdir.FullName, sampleDir));
      return docdir;
    }

  }
}
