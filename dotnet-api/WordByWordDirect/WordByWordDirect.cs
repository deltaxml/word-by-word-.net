﻿// Copyright (c) 2013 DeltaXML Ltd.  All rights reserved.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Add System IO for file handling.
using System.IO;

// Add the main DeltaXML .Net API
using DeltaXML.CoreS9Api;

// Add in the exceptions that we explicitly catch within this sample
using DXPConfigurationException = com.deltaxml.core.DXPConfigurationException;
using InvalidPipelineException = com.deltaxml.core.InvalidPipelineException;
using ParserInstantiationException = com.deltaxml.core.ParserInstantiationException;
using PipelinedComparatorException = com.deltaxml.core.PipelinedComparatorException;

using PipelinedComparatorS9Exception = com.deltaxml.cores9api.PipelinedComparatorS9Exception;

using ClassNotFoundException = java.lang.ClassNotFoundException;
using MalformedURLException = java.net.MalformedURLException;

using SaxonApiException = net.sf.saxon.s9api.SaxonApiException;

namespace DotNetSample
{
  class WordByWordDirect
  {
    public static void Main(string[] args)
    {
      // Find the top level samples directory (whose relative location can vary when run through Visual Studio).
      DirectoryInfo docdir= locateSamplesDirectory("WordByWord");

      // A boolean parameter which corresponds to a similar parameter in DXP
      bool convertToHTML = false;
    
      // Generating a comparison pipeline
      PipelinedComparatorS9 pc = new PipelinedComparatorS9();

      IList<object> inputFilters= new List<object>();
      inputFilters.Add(typeof(com.deltaxml.pipe.filters.NormalizeSpace));
      inputFilters.Add(typeof(com.deltaxml.pipe.filters.dx2.wbw.WordInfilter));

      IList<object> outputFilters= new List<object>();
      ParameterizedFilterS9 orphans= new ParameterizedFilterS9(typeof(com.deltaxml.pipe.filters.dx2.wbw.OrphanedWordOutfilter));
      orphans["orphanedLengthLimit"] = "2";
      orphans["orphanedThresholdPercentage"] = "20";
      outputFilters.Add(orphans);
      outputFilters.Add(typeof(com.deltaxml.pipe.filters.dx2.wbw.WordOutfilter));
      if (convertToHTML) {
          outputFilters.Add(new FileInfo(Path.Combine(docdir.FullName, "convert-to-html.xsl")));
      }
      pc.setInputFilters(inputFilters);
      pc.setOutputFilters(outputFilters);
      pc.setOutputProperty(net.sf.saxon.s9api.Serializer.Property.INDENT, "yes");
      pc.setComparatorFeature("http://deltaxml.com/api/feature/isFullDelta", true);

      // Provide variables for each of the file-based compare method arguments
      FileInfo inFile1= new FileInfo(Path.Combine(docdir.FullName, "input1.xml"));
      FileInfo inFile2= new FileInfo(Path.Combine(docdir.FullName, "input2.xml"));
      FileInfo outFile= new FileInfo(Path.Combine(docdir.FullName, "result.xml"));

      // Run the compare (with pre/post commentry)
      Console.WriteLine("Comparing files '"+inFile1+"' and '"+inFile2+"'");
      try {
        pc.compare(inFile1, inFile2, outFile);
      } catch (PipelinedComparatorS9Exception e) {
        handleComparisonException(e);
      } catch (IOException e) {
        handleComparisonException(e);
      }
      Console.WriteLine("Storing results in file '"+outFile+"'");
  
      // Enable the messages printed to the console/terminal when run from Visual Studio to be read.
      Console.WriteLine();
      Console.WriteLine("Press return to continue");
      Console.ReadLine();
    }

    private static void handleComparisonException(Exception e)
    {
      Console.WriteLine("Caught an exception when performing the comparison.");
      handleException(e);
    }

    private static void handleException(Exception e)
    {
      Console.WriteLine(e.Message);
      Console.WriteLine(e.StackTrace);

      // Enable the error messages printed to the console/terminal when run from Visual Studio to be read.
      Console.WriteLine();
      Console.WriteLine("Press return to continue");
      Console.ReadLine();

      Environment.Exit(1);
    }

    private static DirectoryInfo locateSamplesDirectory(String sampleDir)
    {
      DirectoryInfo docdir = new DirectoryInfo(".");
      while (docdir.Exists && docdir.Name != "samples")
      {
        docdir = docdir.Parent;
      }
      docdir = new DirectoryInfo(Path.Combine(docdir.FullName, sampleDir));
      return docdir;
    }

  }
}
