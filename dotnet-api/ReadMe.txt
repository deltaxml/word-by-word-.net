This directory contains .NET code for the WordByWord sample.  As well as using the command-line
driver, it is also possible to use the DeltaXML Core .NET API to invoke comparison.  In particular the
API can be used with the same DXP files that are used for command-line use.  This enables a configured
pipeline to be loaded from an XML file.

Alternatively, the Pipeline and its filters can be defined using a range of API methods. 

For each of these two approaches we have provided a Visual Studio solution file and a
directory containing the C# source code and Visual Studio project files.

  WordByWordUsingDXP.sln
  WordByWordUsingDXP        This solution/directory uses a DXP file to define the word-by-word pipline

  WordByWordUsingDCP\WordByWordUsingDCP.sln
  WordByWordUsingDCP        This solution/directory uses a DCP file to define the word-by-word pipline

  WordByWordDirect.sln
  WordByWordDirect          This solution/directory defines the word-by-word pipeline directly using the API.

When you have Microsoft Visual Studio installed, the sample can be loaded by 
double-clicking the appropriate solution or '.sln' file.

