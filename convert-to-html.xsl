<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
                exclude-result-prefixes="#all"
                version="2.0">
  
  <!-- identity transform -->
  <xsl:template match="node() | @*" mode="#all">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="/">
    <html>
      <head>
        <title>Word By Word Demo Output</title>
        <style type="text/css">
          <xsl:text>
            span.A {
              background-color: #FF5555;
            }
            span.B {
              background-color: #90EE90;
            }
          </xsl:text>
        </style>
      </head>
      <body>
        <xsl:apply-templates select="//para"/>
      </body>
    </html>
  </xsl:template>
  
  <xsl:template match="para">
    <p><xsl:apply-templates select="node()"/></p>
  </xsl:template>
  
  <xsl:template match="deltaxml:textGroup">
    <xsl:if test="deltaxml:text[@deltaxml:deltaV2='A']">
      <span class="A"><xsl:apply-templates select="deltaxml:text[@deltaxml:deltaV2='A']/text()"/></span>
    </xsl:if>
    <xsl:if test="deltaxml:text[@deltaxml:deltaV2='B']">
      <span class="B"><xsl:apply-templates select="deltaxml:text[@deltaxml:deltaV2='B']/text()"/></span>
    </xsl:if>
  </xsl:template>
  
</xsl:stylesheet>
