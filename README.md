# Word by Word

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/word-by-word`.*

---

How to perform a detailed comparison on textual changes using filters included with XML Compare.

This document describes how to run the sample. For concept details see: [Word by Word Text Comparison](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/word-by-word-text-comparison)

## Running the Sample

The sample code illustrates three methods for performing word by word text comparison. The first method uses the Pipelined Comparator, and specifies input and output filters with a DXP file (identified by the wbw configuration id). The remaining two methods use the Document Comparator, the first of these uses a DCP file (configuration-id dcp-wbw) to configure the comparator pipeline, whilst the second uses the C# API.

### Pipelined Comparator (DXP)

To run just the Pipelined Comparator sample and produce the output files *non-wbw-result.html*, *wbw-result.html* and *orphaned-words-result.html* in the *result/PipelinedComparator* directory, run the *run.bat* batch file. Alternatively the commands are:

	..\..\bin\deltaxml.exe compare wbw input1.xml input2.xml non-wbw-result.html
	..\..\bin\deltaxml.exe compare wbw input1.xml input2.xml wbw-result.html word-by-word=true
	..\..\bin\deltaxml.exe compare wbw input1.xml input2.xml orphaned-words-result.html word-by-word=true orphaned-words=true
	
### Document Comparator (DCP)
To run the Document Comparator DCP sample, run the *run-dcp.bat* batch file. Alternatively, the commands to run only the Document Comparator DCP sample code are the same as for the DXP sample above, but the 'dcp-wbw' configuration-id is used instead of 'wbw', for example:
	
	..\..\bin\deltaxml.exe compare dcp-wbw input1.xml input2.xml non-wbw-result.html
	
If you wish to see the xml delta file result rather than an html result for the Pipelined Comparator samples, simply add the parameter 'convert-to-html=false' to the end of any of the commands.

### Document Comparator (API)
We provide Visual Studio solution (.sln) files for the C# samples in the dotnet-api directory. The Visual Studio project (.csproj) files may be found within each of the three samples directories.

# **Note - .NET support has been deprecated as of version 10.0.0 **